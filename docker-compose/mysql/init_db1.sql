CREATE TABLE `todos` (
                         `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                         `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
                         `deleted_at` timestamp NULL DEFAULT NULL,
                         `created_at` timestamp NULL DEFAULT NULL,
                         `updated_at` timestamp NULL DEFAULT NULL,
                         PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `todos` VALUES (1,'cd8a4dd8-42a9-4c8e-91c7-1c973fa486b7','Breakfast',NULL,'2022-06-21 10:16:23','2022-06-21 10:16:23'),(2,'6098f792-b80d-43a2-8759-1b29610c819f','Work',NULL,'2022-06-21 10:16:26','2022-06-21 10:16:26'),(3,'2bfb73ae-102b-4c39-96af-de9fb0332520','Lunch',NULL,'2022-06-21 10:16:43','2022-06-21 10:16:43'),(4,'565fbf1a-c7c4-482f-a206-535e8e4735c6','Power training',NULL,'2022-06-21 10:16:49','2022-06-21 10:16:49'),(5,'73f3e3ba-edd7-4a3a-91e5-9d460fac384a','Rest',NULL,'2022-06-21 10:17:08','2022-06-21 10:17:08');
