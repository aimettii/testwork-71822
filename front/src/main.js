import Vue from 'vue'
import App from './App.vue'

import 'bootstrap/dist/css/bootstrap.min.css'

Vue.config.productionTip = false

window.$ = window.jQuery = require('jquery');
require('bootstrap');


new Vue({
  render: h => h(App),
}).$mount('#app')
