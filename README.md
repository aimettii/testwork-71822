# Todolist - TestWork 71822

## Project setup
```
1. Clone repo and cd to root directory
```
```
2. cp .env.example .env
```
```
3. docker-compose build app
```
```
4. docker-compose up -d
   Output:
    Creating todolist-db    ... done
    Creating todolist-app   ... done
    Creating todolist-nginx ... done
```
```
5. docker-compose ps
    Output: 
        todolist-app     docker-php-entrypoint php-fpm    Up      9000/tcp
        todolist-db      docker-entrypoint.sh mysqld      Up      3306/tcp, 33060/tcp
        todolist-nginx   /docker-entrypoint.sh ngin ...   Up      0.0.0.0:8000->80/tcp
```
```
6. docker-compose exec app composer install
```
```
7. docker-compose exec app php artisan key:generate
```
### http://localhost:8000 - должно отобразиться Laravel приложение
```
docker-compose down - удалить контейнеры
```

## Building frontend
```
1. cd front
```
```
2. npm install
```

### Compiles and hot-reloads for development
```
3. npm run serve
```

### http://localhost:8080/ доступ к SPA приложению
