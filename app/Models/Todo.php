<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;

/**
 * Class Todo
 * @package App\Models
 * @version June 21, 2022, 9:06 am UTC
 *
 * @property uuid $uuid
 * @property string $name
 */
class Todo extends Model
{
    use SoftDeletes;

    public $table = 'todos';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'uuid',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * Autofill the uuid field when the model instance is being created.
     */
    protected static function boot()
    {
        parent::boot();
        self::creating(function($todoInstance) {
            $todoInstance->uuid = (string) Str::uuid();
        });
    }

    /**
     * Returns route model key name
     * Overridden to uuid
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toFormattedDateString();
    }
}
